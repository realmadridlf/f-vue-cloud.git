export const rule = {
        day: [
        {
            type:  'number' ,
            min: 0.5,
            max: 99,
            required: true,
            message: '请输入0.5-99',
            trigger: 'change',
        },
    ],
    type: [
        {
            type:  'number' ,
            required: true,
            message: '请选择请假类型',
            trigger: 'change',
        },
    ],
    startTime: [
        {
            type: 'string',
            required: true,
            message: '请选择开始时间',
            trigger: 'change',
        },
    ],
    endTime: [
        {
            type: 'string',
            required: true,
            message: '请选择结束时间',
            trigger: 'change',
        },
    ],
    remark: [
        {
            type: 'string' ,
            min: 2,
            max: 200,
            required: true,
            message: '请输入2-200位请假原因',
            trigger: 'change',
        },
    ]
}
