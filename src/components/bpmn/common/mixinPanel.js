import showConfig from '../flowable/showConfig';
//第一种获取target值的方式，通过vue中的响应式对象可使用toRaw()方法获取原始对象
import { toRaw } from '@vue/reactivity';
export default {
	components: {},
	props: {
		modeler: {
			type: Object,
			required: true,
		},
		element: {
			type: Object,
			required: true,
		},
		categorys: {
			type: Array,
			default: () => [],
		},
	},
	watch: {
		'formData.id': function (val) {
			this.updateProperties({ id: val });
		},
		'formData.name': function (val) {
			this.updateProperties({ name: val });
		},
		'formData.documentation': function (val) {
			if (!val) {
				this.updateProperties({ documentation: [] });
				return;
			}
			const documentationElement = this.modeler.get('moddle').create('bpmn:Documentation', { text: val });
			this.updateProperties({ documentation: [documentationElement] });
		},
	},
	methods: {
		updateProperties(properties) {
			const state = toRaw(this.element);
			this.modeler.get('modeling').updateProperties(state, properties);
		},
	},
	computed: {
		elementType() {
			const bizObj = this.element.businessObject;
			return bizObj.eventDefinitions ? bizObj.eventDefinitions[0].$type : bizObj.$type;
		},
		showConfig() {
			return showConfig[this.elementType] || {};
		},
	},
};
