import customContextPad from './customContextPad'

export default {
    __init__: ['customContextPad'],
    customContextPad: ['type', customContextPad]
}