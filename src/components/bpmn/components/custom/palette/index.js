// custom/index.js
import customPalette from './customPalette'

export default {
    __init__: ['customPalette'],
    customPalette: ['type', customPalette]
}