import { request } from '/@/utils/request';
import { SYS_API } from '/@/api/constant';


export interface LeaveApply {
	id: number, // id
	day: number, // 请假天数
	type: number | null, // 请假类型
	startTime: string, // 开始时间
	endTime: string, // 结束时间
	remark: string, // 请假原因
	status: number, // 流程状态
	processId: string, // 流程实例id
}

/**
 * 添加
 * @param data 数据
 * @returns 实体
 */
export function add(data: LeaveApply) {
	return request.post(SYS_API + '/leaveApply', data);
}

/**
 * 修改
 * @param data 数据
 * @returns 修改数量
 */
export function update(data: LeaveApply) {
	return request.put(SYS_API + '/leaveApply', data);
}

/**
 * 提交流程
 * @param id id
 * @returns 流程实例id
 */
export function commit(id: number) {
	return request.post(SYS_API + '/commit', {id});
}

/**
 * 根据id删除
 * @param id 字典id
 * @returns 修改数量
 */
export function deleteById(id: number) {
	return request.delete(SYS_API + '/leaveApply', { data: { id } });
}

/**
 * 详情
 * @param id id必传
 * @returns 返回详情
 */
export function selectById(id: number) {
	return request.get(SYS_API + '/leaveApply?id=' + id);
}

/**
 * 列表查询
 * @param params 要传的参数值，非必传
 * @returns 返回接口数据
 */
export function selectPage(params: object) {
	return request.post(SYS_API + '/leaveApply/page', { ...params, searchCount: true });
}
