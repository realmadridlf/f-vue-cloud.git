import { request } from '/@/utils/request';
import { FLOW_API } from '/@/api/constant';


export interface FlowTask {
    assigneeId:     null;
    assigneeName:   null;
    candidate:      null;
    category:       null;
    comment:        null;
    createTime:     string;
    deployId:       string;
    deptName:       null;
    duration:       string;
    executionId:    null;
    finishTime:     string;
    hisProcInsId:   null;
    procDefId:      null;
    procDefKey:     null;
    procDefName:    string;
    procDefVersion: number;
    procInsId:      string;
    procVars:       null;
    startDeptName:  null;
    startUserId:    null;
    startUserName:  null;
    taskDefKey:     null;
    taskId:         null;
    taskLocalVars:  null;
    taskName:       null;
}

/**
 * 添加
 * @param data 数据
 * @returns 实体
 */
export function add(data: FlowDesign) {
	return request.post(FLOW_API + '/task', data);
}


/**
 * 根据id删除
 * @param id 字典id
 * @returns 修改数量
 */
export function deleteById(id: number) {
	return request.delete(FLOW_API + '/task', { data: { id } });
}

/**
 * 详情
 * @param id id必传
 * @returns 返回详情
 */
export function selectById(id: number) {
	return request.get(FLOW_API + '/task?id=' + id);
}

/**
 * 列表查询
 * @param params 要传的参数值，非必传
 * @returns 返回接口数据
 */
export function selectPage(params: object) {
	return request.post(FLOW_API + '/flow/task/myProcess', { ...params, searchCount: true });
}


